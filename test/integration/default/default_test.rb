# encoding: utf-8

# InSpec tests for gitlab-cadvisor package

control 'general-pkg-checks' do
  impact 1.0
  title 'General tests for gitlab-cadvisor package'
  desc '
    This control ensures that:
      * gitlab-cadvisor package version 0.0.2 is installed

  '
  describe package('gitlab-cadvisor') do
    it { should be_installed }
    its ('version') { should eq '0.0.2' }
  end
end

control 'general-service-checks' do
  impact 1.0
  title 'General tests for cadvisor service'
  desc '
    This control ensures that:
      * cadvisor service is installed, enabled and running
      * cadvisor is listening on 0.0.0.0:9101
  '
  describe service('cadvisor') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end

  describe port('9101') do
    its('processes') { should eq ['cadvisor'] }
    its('protocols') { should eq ['tcp'] }
    its('addresses') { should eq ['::'] }
  end
end

control 'cadvisor-checks' do
  impact 1.0
  title 'cAdvisor tests as a service'
  desc '
    This control ensures that:
      * cadvisor service is returning metrics on port 9101
  '
  describe bash('curl -sSL http://127.0.0.1:9101/metrics') do
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should match (/# TYPE cadvisor_version_info gauge/) }
    its('stderr.strip') { should eq '' }
  end
end
